﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class KnifeScript : MonoBehaviour
{

    [SerializeField] Rigidbody rigidbody;
    private float force;
    private float torque;
    private float airTime;
    Vector2 startSwipe;
    Vector2 endSwipe;

    SceneScript sceneController;
    ScoreScript score;
    [SerializeField] GameObject userInterface;
    [SerializeField] GameObject activeUI;
    [SerializeField] GameObject pauseUI;


    // Start is called before the first frame update
    void Start()
    {
        force = 5f;
        torque = 25f;
        rigidbody.isKinematic = true;
        sceneController = userInterface.GetComponent<SceneScript>();
        score = userInterface.GetComponent<ScoreScript>();
        activeUI.SetActive(true);
        pauseUI.SetActive(false);
    }

    void FixedUpdate() {
        if(activeUI.activeSelf){
            if(rigidbody.isKinematic){
                if(Input.GetMouseButtonDown(0)){
                    startSwipe = Camera.main.ScreenToViewportPoint(Input.mousePosition);
                }
                if(Input.GetMouseButtonUp(0)){
                    endSwipe = Camera.main.ScreenToViewportPoint(Input.mousePosition);
                    Swipe();
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {   
        SwitchUI();
    }

    void Swipe(){


        rigidbody.isKinematic = false;

        airTime = Time.time;

        Vector2 completeSwipe = endSwipe - startSwipe;

        rigidbody.AddForce(completeSwipe * force, ForceMode.Impulse);
        if(CClockwiseSpin(completeSwipe)){
            rigidbody.AddTorque(0f, 0f, torque, ForceMode.Impulse);
        }
        else{
            rigidbody.AddTorque(0f, 0f, -torque, ForceMode.Impulse);
        }
        // Debug.Log(endSwipe + " - " + startSwipe + " = " + completeSwipe);
    }

    bool CClockwiseSpin(Vector2 completeSwipe){
        if(completeSwipe.x > 0)
            return false;
        return true;
    }

    void OnTriggerEnter(Collider col){

        if(col.tag == "platform"){
            rigidbody.isKinematic = true;
            score.AddScore();
        }
        else{
            sceneController.Pause();
            SwitchUI();
        }
    }

    void OnCollisionEnter(){
        float timeInAir = Time.time - airTime;

        if(!rigidbody.isKinematic && timeInAir > .025f){
            sceneController.Pause();
            SwitchUI();
        }
    }

    void SwitchIsKinematic(){
        rigidbody.isKinematic = !rigidbody.isKinematic;
    }

    void Restart(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void SwitchUI(){
        activeUI.SetActive(!sceneController.GetPauseStatus());
        pauseUI.SetActive(sceneController.GetPauseStatus());

        if(sceneController.GetPauseStatus()){
            rigidbody.isKinematic = false;
        }
        else{
            Time.timeScale = 1f; 
        }
    }

}
