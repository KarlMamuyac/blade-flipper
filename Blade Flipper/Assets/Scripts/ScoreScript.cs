﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    [SerializeField] Text scoreText;
    private int currentScore;
    private int highscore;

    // Start is called before the first frame update
    void Start()
    {
        currentScore = -1;
        highscore = PlayerPrefs.GetInt("Highscore", 0);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void SetHighscore(){
        if(currentScore > highscore){
            PlayerPrefs.SetInt("Highscore", currentScore);
        }
    }

    public void AddScore(){
        currentScore++;
        SetHighscore();
        scoreText.text = currentScore.ToString();
    }

    public void ResetScore(){

    }
}
