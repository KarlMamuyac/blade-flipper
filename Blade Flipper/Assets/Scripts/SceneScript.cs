﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneScript : MonoBehaviour
{
    bool isPaused;
    // Start is called before the first frame update
    void Start()
    {
        isPaused = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayDefault(){
        SceneManager.LoadScene("Defaultplay", LoadSceneMode.Single);
    }

    public void Pause(){
        isPaused = true;
    }

    public void Reload(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Play(){
        isPaused = false;
    }

    public bool GetPauseStatus(){
        return isPaused;
    }
}
